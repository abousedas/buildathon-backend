export default {
  region: 'eu-central-1',
  uploadExpiresIn: 180,
  downloadExpiresIn: 86400,
};
